var scrollTop = 0;
var scrollBlocked = false;
var contOffset = $('#how-it-works').offset();
var howIiWorkGoto = true;

$(document).ready(function () {
    // if($(".parallax").length) {
    //     $('.parallax').parallax();
    // }

    // header menu
    $('.header__menu-btn').click(function(e){
        e.preventDefault();

        $(this).toggleClass('header__menu-btn--active');
        $('.header__nav').toggleClass('header__nav--active');
        $('.header').toggleClass('header--active');
    });

    $(document).on('click',function(e){
        if(!$(e.target).closest('.header__menu-btn,.header__nav').length) {
            $('.header__menu-btn').removeClass('header__menu-btn--active');
            $('.header__nav').removeClass('header__nav--active');
            $('.header').removeClass('header--active');
        }
    });

    $('.main-filters__gender-link').click(function (e) {
        e.preventDefault();

        var activeClass = 'main-filters__gender-link_active',
            item = '.main-filters__gender-item',
            linkItem = '.main-filters__gender-link';

        $(this).addClass(activeClass).parents(item).siblings(item).find(linkItem).removeClass(activeClass);
    });

    $('.main-filters__add-item').click(function () {
        $(this).addClass('main-filters__add-item_active').siblings('.main-filters__add-item').removeClass('main-filters__add-item_active');

        $("#overlay-main-filters").fadeIn();
        $(".main-filters__dropdown").addClass('main-filters__dropdown_active');
        $(".main-filters__control-btn").addClass('main-filters__control-btn_active');

        $('.main-filters').addClass('main-filters--active');
    });

    $("#overlay-main-filters , .main-filters__close").click(function () {
        $("#overlay-main-filters").fadeOut();
        $(".main-filters__dropdown").removeClass('main-filters__dropdown_active');
        $('.main-filters__add-item').removeClass('main-filters__add-item_active');
        $(".main-filters__control-btn").removeClass('main-filters__control-btn_active');
        $('.main-filters').removeClass('main-filters--active');
    });

    $('.main-filters__switches-link').click(function (e) {
        e.preventDefault();

        var item = '.main-filters__switches-item',
            link = '.main-filters__switches-link',
            active = 'main-filters__switches-link_active';

        $(this).addClass(active).parents(item).siblings(item).find(link).removeClass(active);
    });

    if(window.innerWidth > 1200) {
        $(".scroll-desktop").mCustomScrollbar();
    }else {
        $(".scroll-mob").mCustomScrollbar();
    }

    $(".main-scroll").mCustomScrollbar({
        mouseWheel:{scrollAmount:300},
        snapAmount:300,
        scrollType: "string"
    });

    $('.main-filters__product-inner').click(function(e){
        e.preventDefault();

        var link = '.main-filters__product-inner',
            item = '.main-filters__product',
            activeClass = 'main-filters__product-inner_active',
            textActive = $(this).text();

        $(this).addClass(activeClass).parents(item).siblings(item).find(link).removeClass(activeClass);
        $('.main-filters__add-item_active .main-filters__add-item-text').text(textActive);
        $('.main-filters__add-item_active').addClass('main-filters__add-item_product-active');
    });

    $('.main-filters__toggle-reset').click(function () {
        var item = $(this).parents('.main-filters__add-item'),
            text = 'Добавить вещь',
            itemTextInner = item.find('.main-filters__add-item-text');

        item.removeClass('main-filters__add-item_product-active');
        itemTextInner.text(text);
    });


    $('.partners__slider').slick({
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        variableWidth: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $('.looks-slider').slick({
        infinite: true,
        speed: 1200,
        slidesToShow: 1,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 8000,
        prevArrow: "<div class='looks-slider__arrow looks-slider__arrow--prev'><i class='icon-back'></i></div>",
        nextArrow: "<div class='looks-slider__arrow looks-slider__arrow--next'><i class='icon-next'></i></div>"
    });

    $('.trends-slider').slick({
        infinite: true,
        speed: 1200,
        slidesToShow: 1,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 8000,
        prevArrow: "<div class='trends-slider__arrow trends-slider__arrow--prev'><i class='icon-back'></i></div>",
        nextArrow: "<div class='trends-slider__arrow trends-slider__arrow--next'><i class='icon-next'></i></div>"
    });

    $('.footer__lang').styler();

    $('.how-it-works__inner').slick({
        arrows: false,
        fade: true,
        dots: true,
        speed: 1500,
        autoplay: true,
        autoplaySpeed: 5000
    });

    //
    $('.acordion__header').click(function(){
      $(this).toggleClass('acordion__header--active');
      $(this).next('.acordion__cont').slideToggle();
    });


    // init Masonry
    var $grid = $('.look-grid').masonry({
      itemSelector: '.look-grid__item',
      gutter: 0,
      percentPosition: true,
      animate: true
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry();
    });

    // page-welcome

    if($('.page-welcome').length) {
        $(window).scroll(function(){
            var scrollTop = $(window).scrollTop();

            if(scrollTop > 100) {
                $('.main-filters').addClass('main-filters--fixed');
                console.log('test')
            }else {
                $('.main-filters').removeClass('main-filters--fixed');
            }
        });
    }

    // range
    $( "#range-things" ).slider({
        range: true,
        min: 1,
        max: 10,
        values: [ 2, 5 ],
        slide: function( event, ui ) {
            $('#range-things-min').text(ui.values[ 0 ]);
            $('#range-things-max').text(ui.values[ 1 ]);
        }
    });
    $('#range-things-min').text($( "#range-things" ).slider( "values", 0 ));
    $('#range-things-max').text($( "#range-things" ).slider( "values", 1 ));

    $( "#range-cost" ).slider({
        range: true,
        min: 2500,
        max: 250000,
        values: [ 2500, 250000 ],
        slide: function( event, ui ) {
            $('#range-cost-min').text(ui.values[ 0 ]);
            $('#range-cost-max').text(ui.values[ 1 ]);
        }
    });
    $('#range-cost-min').text($( "#range-cost" ).slider( "values", 0 ));
    $('#range-cost-max').text($( "#range-cost" ).slider( "values", 1 ));

    // .look-btn-filter
    $('.look-btn-filter').click(function (e) {
        e.preventDefault();

        $(this).siblings('.acordion').slideToggle();
    });
});
